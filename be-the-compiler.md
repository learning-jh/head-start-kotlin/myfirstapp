# Chapter 1 Exercises

## Be the compiler

### A

```kt
fun main(args: Array<String>) {
    var x = 1
    while (x < 10) {
        if (x > 3) {
            println("big x")
        }
    }
}
```

Without an increment step, will infinite-loop.

### B

```kt
fun main(args: Array<String>) {
    val x = 10
    while (x > 1) {
        x = x - 1
        if (x < 3) println("small x")
    }
}
```

Will not compile --- altering `val x`.

### C

```kt
fun main(args: Array<String>) {
    var x = 10
    while (x > 1) {
        x = x - 1
        print(if (x < 3) "small x")
    }
}
```

Won't compile --- if-statement requires `else`.
